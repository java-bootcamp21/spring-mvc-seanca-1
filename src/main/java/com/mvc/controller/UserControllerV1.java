package com.mvc.controller;

import com.mvc.model.User;
import com.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@Controller
@RequestMapping("/v1/customer")
public class UserControllerV1 {

    @Autowired
    private UserService userService;

    @GetMapping
    public String getCustomerListView(Model model){
        model.addAttribute("customers",userService.getUsers());
        return "tailwindcss/customer-list";
    }


    @GetMapping("/register-view")
    public String getRegistrationView(Model model,@RequestParam(value = "userId",required = false) Long id){
        model.addAttribute("countryList", Arrays.asList("Albania","Kosovo","North Macedonia"));
        if(id!=null){
            model.addAttribute("customerForm",userService.getUserById(id));
            model.addAttribute("viewTitle","Customer Update");
        }else {
            model.addAttribute("customerForm", new User());
            model.addAttribute("viewTitle","Customer Registration");
        }

        return "tailwindcss/registration-form";
    }

    @PostMapping("/register")
    public String saveCustomerFormAction(@ModelAttribute("customerForm") User user){
        System.err.println(user);
        if(user.getId()!=null){
            userService.updateById(user.getId(),user);
        }else {
            userService.createUser(user);
        }
        return "redirect:/v1/customer";
    }


    @GetMapping("/delete")
    public String delete(@RequestParam(name = "userId") Long id){
        userService.deleteById(id);
        return "redirect:/v1/customer";
    }
    
    @GetMapping("/details")
    public String getCustomerDetails(Model model) {
    	model.addAttribute("userList",userService.getUsers());
    	return "tailwindcss/customer-details";
    }


}
